export const initialState = { contacts: [] }

const GET_CONTACTS = 'GET_CONTACTS';
const ADD_CONTACT = 'ADD_CONTACT';
const EDIT_CONTACT = 'EDIT_CONTACT';
const DELETE_CONTACT = 'DELETE_CONTACT';


export const addContact = (data) => {
    return {
        type: ADD_CONTACT,
        payload: data
    }
}



export const contactsReducer = (state = initialState, action) => {

    switch (action.type) {
        case GET_CONTACTS:
            return {
                ...state,
                contacts: action.payload
            }
        case ADD_CONTACT:
            return {
                ...state,
                contacts: action.payload
            }
        default: return state;
    }

}
