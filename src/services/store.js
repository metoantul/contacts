import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { contactsReducer } from './redux/duck/users';


const middleware = applyMiddleware(thunk, createLogger());

export default createStore(contactsReducer, middleware)