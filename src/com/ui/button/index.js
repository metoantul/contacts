import React from 'react'
import './style.css'

const Button = ({ name, className, onClick }) => {
    return <button onClick={onClick} className={`button ${className}`}>{name}</button>
}

export default Button
