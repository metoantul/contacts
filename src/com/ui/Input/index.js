import './style.css'

const Input = ({ label, type, placeholder, value, onChange, name }) => {
    return (
        <div className="input-style">
            <label>{label}</label>
            <input
                type={type}
                placeholder={placeholder}
                value={value}
                onChange={onChange}
                name={name}

            />
        </div>
    )
}

export default Input
