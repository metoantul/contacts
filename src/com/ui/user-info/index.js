import React from 'react'
import './style.css'

const UserInfo = ({ name, email, pno }) => {
    return (
        <div className='user-info-wrapper'>
            <span className='span-name span-global'>{name}</span>
            <span className='span-email span-global'>{email}</span>
            <span className='span-pno span-global' >{pno}</span>
        </div>
    )
}

export default UserInfo
