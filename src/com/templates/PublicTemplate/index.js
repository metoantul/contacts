import React from 'react';
import './style.css';

const PublicTemplate = ({ children }) => {
    return (
        <div className='public-template'>
            <div className='public-template-header  public-template-padding'>Header</div>
            <div className='public-template-children'>

                <div>{children}</div>

            </div>
            <div className='public-template-footer public-template-padding'>footer</div>
        </div>
    )
}

export default PublicTemplate
