import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import './style.css'
import PublicTemplate from '../../templates/PublicTemplate'
import Input from '../../ui/Input/index'
import UserInfo from '../../ui/user-info'
import Button from '../../ui/button'
import { addContact } from '../../../services/redux/duck/users';

const MainDashboard = () => {
    const dispatch = useDispatch();

    const contacts = useSelector((state) => state.contacts)
    console.log(contacts);
    const [userData, setUserData] = useState({
        name: '',
        email: '',
        pno: ''
    });
    const onClickHandler = () => {
        dispatch(addContact(userData))
    }


    const dataFieldUpdate = (e) => {
        setUserData({
            ...userData,
            [e.target.name]: e.target.value
        })
    }

    const inputs = [{
        label: 'Name',
        name: 'name',
        placeholder: 'Your name...',
        type: 'text',

    }, {
        label: 'Email',
        name: 'email',
        placeholder: 'Your email...',
        type: 'email'
    }, {
        label: 'Phone',
        name: 'pno',
        placeholder: 'Your phone nunmber...',
        type: 'number'
    }]



    // const removeHandler = (id) => {
    //     let filteredUsers = users.filter((el) => {
    //         return
    //     })
    // }

    // const saveHandler = () => {
    //     setUsers([
    //         ...users,
    //         userData
    //     ])
    //     setUserData({
    //         name: '',
    //         email: '',
    //         pno: ''
    //     })
    // }

    return (
        <PublicTemplate>
            <div className='input-list'>

                {inputs.map((el, i) => {
                    return (
                        <Input key={i}
                            label={el.label}
                            type={el.type}
                            placeholder={el.placeholder}
                            value={userData[el.name]}
                            onChange={dataFieldUpdate}
                            name={el.name}
                        />
                    )
                })}
                <Button name='Save' onClick={onClickHandler} />
            </div>
            <div >
                {contacts.map((el, i) => {
                    return (
                        <div className='users-list-wrapper' key={i}>
                            <UserInfo key={i} name={el.name} email={el.email} pno={el.pno} />
                            <Button name='asd' />
                        </div>
                    )
                })}
            </div>

        </PublicTemplate>
    )
}

export default MainDashboard
