import './assets/styles/global.css'

import MainDashboard from './com/pages/MainDashboard';

function App() {
    return (
        <>
            <MainDashboard />
        </>
    );
}

export default App;
